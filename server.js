import app from './app';
import { debugApp } from './config/debug.config';

const port = process.env.PORT || 5000;

app.listen(port, () => debugApp(`Listenning on ${port}...`));
