import express from 'express';
import cors from 'cors';
import dotenv from 'dotenv';
import config from './config/user.config';
import morgan from 'morgan';
import { urlencoded } from 'express';
import { sendSuccess } from './helpers/senders';
import authRoute from './routes/auth.routes';
import {
  cakes,
  cookies,
  burgers,
  pizzas,
  burgerData,
  pizzaData,
  cookieData,
  cakeData,
} from './routes/Product.Route';
import mongoose from 'mongoose';
import { debugDB } from './config/debug.config';
dotenv.config();
const app = express();
const mongodbUrl = config.MONGODB_URL;
mongoose
  .connect(
    mongodbUrl,
    {
      useNewUrlParser: true,
      useUnifiedTopology: true,
      useCreateIndex: true,
    },
    debugDB('connected....')
  )
  .catch((error) => debugDB('error:', error.reason));
app.use(express.json());
app.use(cors());
app.use(morgan('dev'));
app.use(urlencoded({ extended: true }));
app.get('/', (req, res) => sendSuccess(res, 200, 'welcome to foodCo'));

app.use('/api/auth', authRoute);
app.use('/api/products', burgers);
app.use('/api/products', burgerData);
app.use('/api/products', pizzas);
app.use('/api/products', pizzaData);
app.use('/api/products', cookies);
app.use('/api/products', cookieData);
app.use('/api/products', cakes);
app.use('/api/products', cakeData);
module.exports = app;
