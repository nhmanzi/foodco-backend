const mongoose = require('mongoose');

mongoose
  .connect('mongodb://localhost/test')
  .then(() => console.log('connected...'))
  .catch((error) => console.error('couldnt connect', error));

const BurgerSchema = new mongoose.Schema({
  firstName: String,
  lastName: String,
  email: String,
  password: String,
});

const Burgers = mongoose.model('burger', BurgerSchema);
async function createBurger() {
  const burger = new Burgers({
    firstName: 'olibtr',
    lastName: 'kigali',
    email: 'email@gmail.com',
    password: '1245',
  });

  const results = await burger.save();
  console.log(results);
}

createBurger();
