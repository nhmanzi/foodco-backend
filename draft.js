getRepository = (username) => {
  return new Promise((resolve, reject) => {
    if (username !== 'oliver') return reject(new Error('username is invalid'));
    else setTimeout(() => resolve(['repo1', 'repo2', 'repo2']), 2000);
  })
    .catch((err) => console.log(err.message))
    .then((result) => console.log(result));
};

getRepository('oliverf');

const Joi = require('@hapi/joi');

exports.UserSchema = Joi.object({
  email: Joi.string().email().required(),
  password: Joi.string()
    .regex(/[a-zA-Z0-9]/)
    .required(),
});

const Joi = require('@hapi/joi');

const userSchema = Joi.object({
  email: Joi.string().email().required(),
  password: Joi.string().regex(/[a-zA-Z0-9]{6,128}/),
});

module.exports = userSchema;
