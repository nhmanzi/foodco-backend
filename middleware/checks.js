import userSchema from '../models/user.schema';
import { debugError } from '../config/debug.config';
import { sendError } from '../helpers/senders';

exports.checkLogUp = (req, res, next) => {
  const { error } = userSchema.validate(req.body);
  if (error) {
    debugError(error);
    sendError(res, 400, error);
  } else next();
};
