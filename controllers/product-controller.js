import { debugApp, debugError } from '../config/debug.config';
import { burgers, cakes, cookies, pizza } from '../models/data';
export const burgerDetail = (req, res) => {
  res.send(burgers);
};
export const singleBurger = (req, res) => {
  const burgerId = parseInt(req.params.id);
  debugApp('id>>>', burgerId);
  const singleBurger = burgers.find((c) => c.id === burgerId);
  debugApp('data>>>', singleBurger);
  singleBurger
    ? res.send(singleBurger)
    : res.status(404).send({ msg: 'product not found' });
};
export const pizzaDetail = (req, res) => {
  res.send(pizza);
};
export const singlePizza = (req, res) => {
  const pizzaId = parseInt(req.params.id);
  const singlePizza = pizza.find((c) => c.id === pizzaId);
  singlePizza
    ? res.send(singlePizza)
    : res.status(404).send({ msg: 'product not found' });
};
export const cakeDetail = (req, res) => {
  res.send(cakes);
};
export const singlecake = (req, res) => {

  const cakeId = parseInt(req.params.id);
  debugApp('identity:',cakeId)
  const singlecake = cakes.find((c) => c.id === cakeId);
  debugApp('identity cake:',singlecake)
  singlecake
    ? res.send(singlecake) 
    : res.status(404).send({ msg: 'product not found' });
};
export const cookiesDetail = (req, res) => {
  res.send(cookies);
};
export const singlecookie = (req, res) => {
  const cookieId = parseInt(req.params.id);
  const singlecookie = cookies.find((c) => c.id === cookieId);
  singlecookie
    ? res.send(singlecookie)
    : res.status(404).send({ msg: 'product not found' });
};
