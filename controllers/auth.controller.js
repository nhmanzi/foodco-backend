import { compareSync, hashSync } from 'bcrypt';
import User from '../models/user.schema';
import { sign } from 'jsonwebtoken';
import JoiUserSchema from '../models/user.model';
import { debugApp, debugDB, debugError } from '../config/debug.config';
import { sendSuccess, sendError } from '../helpers/senders';

exports.signUp = async (req, res) => {
  const { firstName, lastName, email, password, retype } = req.body;
  if (password === retype) {
    const match = await User.findOne({ email: email });

    if (!match) {
      const pwd = hashSync(password, 10);
      const user = new User({ firstName, lastName, email, password: pwd });
      debugApp(newUser);
      const newUser = await user.save();
      sendSuccess(res, 201, newUser);
    } else sendError(res, 401, 'email already exist');
  } else sendError(res, 401, 'password doesnt match');
};

exports.logIn = async (req, res) => {
  const { email, password } = req.body;
  debugError('infosnow', req.body);
  const match = await User.findOne({ email: email });
  if (match) {
    const pwd = compareSync(password, match.password);
    if (pwd) {
      const token = sign(
        { email: match.email, name: `${match.firstName} ${match.lastName} `,isAdmin:match.isAdmin },
        process.env.JWT_KEY,
        {
          expiresIn: '1hr',
        }
      );
      sendSuccess(res, 200, { token });
      debugApp(`token: ${token}`);
    
    } else {
        sendError(res, 401, 'Invalid Password or Email, try again!');
      debugError('Invalid Email or Password');
    }
  
  } else {
   sendError(res, 401, 'Invalid Email or password, try again!');
    debugError('Invalid Email or Password');
  }
};
