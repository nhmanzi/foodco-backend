// import Joi from '@hapi/joi';
import mongoose from 'mongoose';
// const joiUserSchema = Joi.object({
//   firstName: Joi.string().min(3).required(),
//   lastName: Joi.string().min(3).required(),
//   email: Joi.string().email().required(),
//   password: Joi.string()
//     .regex(/[a-zA-Z0-9]{6,128}/)
//     .required(),
//   retype: Joi.string().regex(/[a-zA-Z0-9]{6,128}/),
// });
// JoiUserSchema

const userSchema = new mongoose.Schema({
  firstName: { type: String, required: true },
  lastName: { type: String, required: true },
  email: { type: String, required: true, unique: true, dropDups: true },
  password: { type: String, required: true },
  isAdmin: { type: Boolean, default: false },
});
const UserModel = mongoose.model('User', userSchema);

export default UserModel;
