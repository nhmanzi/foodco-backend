
import { debugError } from '../config/debug.config';

class User {
  constructor(fn, ln, email, password) {
    (this.id = this.idGen()),
      (this.firstName = fn),
      (this.lastName = ln),
      (this.email = email),
      (this.password = password);
  }

  idGen() {
    return users.length + 1;
  }
}
module.exports = User;

// const userSchema = new moongose.Schema({
//   firstName: { type: String, required: true },
//   lastName: { type: String, required: true },
//   email: { type: String, required: true, unique: true },
//   password: { type: String, required: true },
//   isAdmin: { type: Boolean, required: true, default: false },
// });
