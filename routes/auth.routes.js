import express from 'express';
import User from '../models/user.schema';
import { compareSync, hashSync } from 'bcrypt';
const router = express.Router();
import { checkLogUp } from '../middleware/checks';
import { signUp, logIn } from '../controllers/auth.controller';
import { debugApp } from '../config/debug.config';
router.post('/signup', checkLogUp, signUp);
router.post('/login', logIn);
router.get('/createAdmin', async (req, res) => {
  debugApp('here we go')
  try {
    const user = new User({
      firstName: 'nshuti',
      lastName: 'manzi',
      email: 'manziolivier250@gmail.com',
      password: hashSync('12345',10),
      isAdmin: true,
    });
    const newUser = await user.save();
    res.send(newUser);
  } catch (error) {
    res.send({ msg: error.message });
  }
});
module.exports = router;
