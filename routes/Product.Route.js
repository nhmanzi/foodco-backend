import express from 'express';
const router = express.Router();
import {
  burgerDetail,
  cakeDetail,
  cookiesDetail,
  pizzaDetail,
  singleBurger,
  singlecake,
  singlecookie,
  singlePizza,
} from '../controllers/product-controller';
export const burgers = router.get('/getBurgers', burgerDetail);
export const burgerData = router.get('/getBurgers/:id', singleBurger);
export const pizzas = router.get('/getPizzas', pizzaDetail);
export const pizzaData = router.get('/getPizzas/:id', singlePizza);
export const cakes = router.get('/getCakes', cakeDetail);
export const cakeData = router.get('/getCakes/:id', singlecake);
export const cookies = router.get('/getCookies', cookiesDetail);
export const cookieData = router.get('/getCookies/:id', singlecookie);
