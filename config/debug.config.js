const debug = require('debug');

exports.debugApp = debug('app:app');
exports.debugDB = debug('app:db');
exports.debugError = debug('app:error');
