exports.sendSuccess = (res, status, data) => {
  if (data) {
    return res.status(status).send({
      data,
    });
  } else {
    return res.status(status).send({
      message: 'Success',
    });
  }
};
exports.sendError = (res, status, error) => {
  return res.status(status).send({message:error}  );
};
